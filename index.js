function transformJSXAttribute (path, options) {
  const target = options.attributes[path.node.name.name];
  transformJSXObjectProperty(path, target);
}

function transformJSXExpressionContainer(path, options) {
  path.traverse({
    ObjectProperty(path) {
      if (!options.objectKeysOfAttributeValues) {
        return;
      }
      const target = options.objectKeysOfAttributeValues[path.node.key.name || path.node.key.value];
      transformObjectProperty(path, target);
    }
  });
}

function transformJSXSpreadAttribute (path, options) {
  path.traverse({
    ObjectProperty(path) {
      const target = options.attributes[path.node.key.name || path.node.key.value];
      transformObjectProperty(path, target);
    }
  });
}

function transformJSXObjectProperty (path, target) {
  if (target === null) {
    path.remove();
  }
  if (target) {
    const prop = path.node.name;
    if (prop.name) {
      prop.name = target;
    }
    if (prop.value) {
      prop.value = target;
    }
  }
}

function transformObjectProperty (path, target) {
  if (target === null) {
    path.remove();
  } else if (target && path.node.key.name) {
    path.node.key.name = target;
  } else if (target && path.node.key.value) {
    path.node.key.value = target;
  }
}

const transformers = {
  JSXAttribute: transformJSXAttribute,
  JSXSpreadAttribute: transformJSXSpreadAttribute,
  JSXExpressionContainer: transformJSXExpressionContainer,
}

module.exports = function() {
  return {
    name: 'remove-jsx-attribute',
    visitor: {
      'JSXAttribute|JSXSpreadAttribute|JSXExpressionContainer': function (path) {
        if (path.node.type) {
          transformers[path.node.type](path, this.opts);
        }
      },
    }
  }
};
