import React from 'react';

export default function() {
  // this one should not change.
  const obj = { foo4: 'foo4' };

	return (
    <h1
      foo="foo"
      foo2="foo2"
      foo3="foo3"
      {...{foo4: 'foo4', foo5: 'foo5', 'foo-8': 'foo-8'}}
      prop={{
        foo6: 'foo6',
        foo7: 'foo7',
        'foo-8': 'foo-8',
        'foo-9': 'foo-9',
      }}
    >Hello World</h1>
  );
}
