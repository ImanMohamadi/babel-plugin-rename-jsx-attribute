var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React from 'react';

export default function () {
  // this one should not change.
  const obj = { foo4: 'foo4' };

  return React.createElement(
    'h1',
    _extends({
      bar: 'foo',
      bar2: 'foo2'
    }, { bar4: 'foo4', 'bar-8': 'foo-8' }, {
      prop: {
        bar6: 'foo6',

        'bar-8': 'foo-8'
      }
    }),
    'Hello World'
  );
}
