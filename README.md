# babel-plugin-rename-jsx-attribute

Renames the react element attribute in jsx, supports JSX attributes, spread operator attributes and supports transforming of object keys that are values of JSX attributes.

```
npm install babel-plugin-rename-jsx-attribute --save-dev
```

can use like:
```
{
  "presets": ["react"],
  "plugins": [
    [
      "../index.js",
      {
        "attributes": {
          "foo": "bar",
          "foo2": "bar2",
          "foo3": null,
          "foo4": "bar4",
          "foo5": null
        },
        "objectKeysOfAttributeValues": {
          "foo6": "bar6",
          "foo7": null
        }
      }
    ]
  ]
}
```

Here is a sample input:

```jsx
import React from 'react';

export default function() {
  // this one should not change.
  const obj = { foo4: 'foo4' };

	return (
    <h1
      foo="foo"
      foo2="foo2"
      foo3="foo3"
      {...{foo4: 'foo4', foo5: 'foo5'}}
      prop={{
        foo6: 'foo6',
        foo7: 'foo7'
      }}
    >Hello World</h1>
  );
}
```

Here is the JSX version (imaginary) of compile output: 

```jsx
import React from 'react';

export default function() {
  // this one should not change.
  const obj = { foo4: 'foo4' };

	return (
    <h1
      bar="foo"
      bar2="foo2"
      {...{bar4: 'foo4'}}
      prop={{
        bar6: 'foo6',
      }}
    >Hello World</h1>
  );
}
```
